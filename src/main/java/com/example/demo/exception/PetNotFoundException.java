package com.example.demo.exception;

public class PetNotFoundException extends RuntimeException {
    public PetNotFoundException(Long petId) {
        super(petId.toString());
    }
}
