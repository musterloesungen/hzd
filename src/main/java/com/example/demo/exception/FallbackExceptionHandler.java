package com.example.demo.exception;

import de.hzd.models.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order
@Slf4j
public class FallbackExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> catchAll(final Throwable exception) {
        log.error("{}: {}", exception.getClass().getCanonicalName(), exception.getLocalizedMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse()
                        .message("Internal error")
                        .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                );
    }

}
