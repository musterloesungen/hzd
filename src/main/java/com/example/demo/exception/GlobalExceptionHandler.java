package com.example.demo.exception;

import de.hzd.models.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({
            NotImplementedException.class
    })
    public ResponseEntity<ErrorResponse> onError(Exception exception) {
        log.error("{}: {}", exception.getClass().getCanonicalName(), exception.getLocalizedMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_IMPLEMENTED)
                .body(new ErrorResponse()
                        .message(exception.getLocalizedMessage())
                        .code(HttpStatus.NOT_IMPLEMENTED.value())
                );
    }

    @ExceptionHandler({
            PetNotFoundException.class
    })
    public ResponseEntity<ErrorResponse> onMessageError(Exception exception) {
        log.warn("{} - Pet not found: {}", exception.getClass().getCanonicalName(), exception.getLocalizedMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse()
                        .message("Pet not found")
                        .code(HttpStatus.NOT_FOUND.value())
                );
    }
}
