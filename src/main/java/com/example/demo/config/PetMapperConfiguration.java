package com.example.demo.config;

import com.example.demo.mapper.PetMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PetMapperConfiguration {

    @Bean
    public PetMapper petMapper() {
        return Mappers.getMapper(PetMapper.class);
    }
}
