package com.example.demo.mapper;

import com.example.demo.entity.Pet;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CategoryMapper.class, TagMapper.class})
public interface PetMapper {

    com.example.demo.entity.Pet dto2entity(de.hzd.models.Pet petDTO);

    de.hzd.models.Pet entity2dto(com.example.demo.entity.Pet pet);

    List<de.hzd.models.Pet> entities2dtos(List<Pet> pet);

    void updatePet(com.example.demo.entity.Pet petDTO, @MappingTarget com.example.demo.entity.Pet entity);
}