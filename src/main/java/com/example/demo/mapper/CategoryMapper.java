package com.example.demo.mapper;

import org.mapstruct.Mapper;

@Mapper
public interface CategoryMapper {

    com.example.demo.entity.Category dto2entity(de.hzd.models.Category categoryDTO);

    de.hzd.models.Category entity2dto(com.example.demo.entity.Category category);
}