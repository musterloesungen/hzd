package com.example.demo.mapper;

import org.mapstruct.Mapper;

@Mapper
public interface TagMapper {

    com.example.demo.entity.Tag dto2entity(de.hzd.models.Tag tagDTO);

    de.hzd.models.Tag entity2dto(com.example.demo.entity.Tag tag);
}
