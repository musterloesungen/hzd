package com.example.demo.service;

import com.example.demo.exception.PetNotFoundException;
import com.example.demo.repository.PetRepository;
import com.example.demo.mapper.PetMapper;
import de.hzd.models.Pet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PetServiceImpl implements PetService {

    private final PetRepository petRepository;
    private final PetMapper petMapper;

    @Override
    public Pet addPet(Pet pet) {
        com.example.demo.entity.Pet savedPetEntity = petRepository.save(petMapper.dto2entity(pet));

        return petMapper.entity2dto(savedPetEntity);
    }

    @Override
    public Pet getPetById(Long petId) {
        final var pet = petRepository
                .findById(petId)
                .orElseThrow(() -> new PetNotFoundException(petId));
        return petMapper.entity2dto(pet);
    }

    @Override
    public List<Pet> findPetsByTags(List<String> tags) {
        return petMapper.entities2dtos(petRepository.findByTagsNames(tags));
    }
}
