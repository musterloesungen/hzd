package com.example.demo.service;

import de.hzd.models.Pet;

import java.util.List;

public interface PetService {
    Pet addPet(Pet pet);

    Pet getPetById(Long petId);

    List<Pet> findPetsByTags(List<String> tags);
}
