package com.example.demo.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class LoggingFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        log.info("Incoming request {} : {}", req.getMethod(), req.getRequestURI());

        // Fortsetzung der Filterkette
        filterChain.doFilter(request, response);

        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader("bla", "bla");

        // Nachfolgende Aktionen nach der Controller-Verarbeitung
        log.info("Outgoing response for {} : {} ({})", req.getMethod(), req.getRequestURI(), resp.getStatus());
    }

}