package com.example.demo.rest;

import com.example.demo.service.PetService;
import de.hzd.api.PetApi;
import de.hzd.models.ModelApiResponse;
import de.hzd.models.Pet;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PetController implements PetApi {

    private final PetService petService;

    @Override
    public ResponseEntity<Pet> addPet(Pet pet) {
        return ResponseEntity.ok(petService.addPet(pet));
    }

    @Override
    public ResponseEntity<Void> deletePet(Long petId, String apiKey) {
        return null;
    }

    @Override
    public ResponseEntity<List<Pet>> findPetsByStatus(String status) {
        return null;
    }

    @Override
    public ResponseEntity<List<Pet>> findPetsByTags(List<String> tags) {
        return ResponseEntity.ok(petService.findPetsByTags(tags));
    }

    @Override
    public ResponseEntity<Pet> getPetById(Long petId) {
        return ResponseEntity.ok(petService.getPetById(petId));
    }

    @Override
    public ResponseEntity<Pet> updatePet(Pet pet) {
        return null;
    }

    @Override
    public ResponseEntity<Void> updatePetWithForm(Long petId, String name, String status) {
        return null;
    }

    @Override
    public ResponseEntity<ModelApiResponse> uploadFile(Long petId, String additionalMetadata, Resource body) {
        return null;
    }
}
